<div align="center">
  <img style="margin: 30px" src="./img/java-logo.png" width="100">
</div>

# Backend-Task

The goal of this task is to create a Java application which serves a REST-API for a simple Task-Manager web-app. To not waste any time, the project [task-manager-backend](https://gitlab.com/hofaphil/task-manager-backend) gets you started and is already pre-configured to a certain level. It uses Spring as application framework and depends on a simple H2 document database. The frontend is already implemented and can be used to visualize the results. To serve it, follow the instructions of the readme of the [task-manager-frontend](https://gitlab.com/hofaphil/task-manager-frontend) repository.

## 🛠️ Task 1: Setup

Follow these steps to deploy the backend:

1. Make sure that you have the Java JDK installed on machine 
2. Clone the project [task-manager-backend](https://gitlab.com/hofaphil/task-manager-backend) and make sure that you checked out the `coding-interview` branch
3. Follow the instructions in the readme of [task-manager-backend](https://gitlab.com/hofaphil/task-manager-backend) and verify that the backend is running (with some errors) ✅

## 💻 Task 2: Implementation

#### Database
Implement the `TaskModel` by using Hibernate annotations. This model should contain all relevant information that is received from the frontend with sends data as a `TaskDTO`.

#### Service
Implement the `TaskService` interface with the `DefaultTaskService`. This service should access the database which is a Spring data-repository. So you can access the DB with the `TaskRepository`.

#### Endpoint
Implement the `TaskEndpoint` class and use the `TaskService` interface for the actual CRUD operations (do not use the database directly). The endpoint needs to provide the following methods:

- Update or insert a new task:
```
POST "/tasks"
TaskDTO updateOrCreateTask(TaskDTO task)
```

- Get all tasks that are currently store in the database:
```
GET "/tasks"
List<TaskDTO> getAllTasks()
```

- Get a specific task identified by the provided id:
```
GET "/tasks/{id}"
TaskDTO getTask(Integer id)
```

- Delete a certain task identified by the id:
```
DELETE "/tasks/{id}"
void deleteTask(Integer id)
```

<div align="center">
  <img style="margin: 30px" src="./img/backend-architecture-diagram.png" width="600">
</div>

## 🪲 Task 3: Testing (optional)
If there is some time remaining, write some unit-tests for your `DefaultTaskService`.\
If no time is left, test your implementation with the provided [frontend](https://gitlab.com/hofaphil/task-manager-frontend).