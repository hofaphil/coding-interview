<div align="center">
  <img style="margin: 50px" src="./img/vue-logo.png" width="100">
</div>

# Frontend-Task

The goal of this task is to create a simple Vue.js web-application that serves as a simple Task Manager application. The component framework Vuetify is already preinstalled. To not waste too much time with the setup, the project [task-manager-frontend](https://gitlab.com/hofaphil/task-manager-frontend) gets you started. The backend is provided by the [task-manager-backend](https://gitlab.com/hofaphil/task-manager-backend) and can be deployed as described in the readme of the repository later (if needed).

## 🛠️ Task 1: Setup

The following steps need to be executed to make the frontend project work:

1. To develop the frontend application you need Git and Node.js installed on your machine
2. Clone the project [task-manager-frontend](https://gitlab.com/hofaphil/task-manager-frontend) and checkout the branch `coding-interview`
3. Verify that the app can be served on your machine (follow the steps listed in the readme of the frontend repository) ✅

## 💻 Task 2: Build the task-list component

Build a task-list component with the following requirements:

1. There is a list containing all not-completed tasks
2. There is a list containing all completed tasks
3. The status of a task (completed/not-completed) needs to be visible
4. The title of a task need so be visible
5. A task can be deleted 
6. A task can be completed
7. A new task can be added

Try to build everything modular and use suitable components. Styling is not important

### Inspiration

*!!! Your app does not have to look like this !!!*

<img src="./img/frontend-inspiration.png" width="400" style="border-radius: 0.2cm">

## 📡 Task 3: Call the real API (optional)
Replace the mock `TaskApi.ts` with real backend calls. Try to use e.g., the javascript fetch-API. To test your implementation, you need to have the [backend](https://gitlab.com/hofaphil/task-manager-backend) running!
- Implement the `getAllTasks` call
- Implement the `updateTask` call
- Implement the `deleteTask` call